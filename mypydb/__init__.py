import os
import yaml

from mypydb.database import Database
from mypydb.errors import MissingDatabaseConfiguration

__all__ = ['connect', 'set_conn_config']


_CONNECTIONS = None


def connect(conn_name=None, **kwargs):

    if conn_name is not None:
        if _CONNECTIONS is None:
            raise MissingDatabaseConfiguration(
                'Env variable "DB_CONNECTIONS_CONFIG" '
                'for database connections configuration file not found. '
                'Add it or add configuration with "set_conn_config" method.')

        connection_config = _CONNECTIONS.get(conn_name)

        if connection_config is None:
            raise MissingDatabaseConfiguration(
                'Database ID with name: {} does not '
                'exist in connections configuration'.format(name))

        connection_config.update(kwargs)

        return Database(**connection_config).connect()
    else:
        return Database(**kwargs).connect()


def set_conn_config(config_file):
    global _CONNECTIONS
    _CONNECTIONS = _read_config(config_file)


def _read_config(config_file):
    with open(config_file, 'r') as stream:
        return yaml.safe_load(stream)


conn_config_fl = os.environ.get('DB_CONNECTIONS_CONFIG')
if conn_config_fl is not None:
    _CONNECTIONS = _read_config(conn_config_fl)

