
__all__ = ['MissingWhereCondition', 'MissingDatabaseConfiguration']


class MissingWhereCondition(Exception):
    pass


class MissingDatabaseConfiguration(Exception):
        pass

