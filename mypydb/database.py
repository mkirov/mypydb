from pymysql import connect

from mypydb import constants


__all__ = ['Database']


class Database:

    def __init__(self, **kwargs):
        self.connection = None
        self.db_conf = kwargs
        self.db_conf['charset'] = kwargs.get('charset', constants.DEFAULT_CHARSET)
        self.db_conf['port'] = kwargs.get('port', constants.DEFAULT_DB_PORT)
        self.db_conf['autocommit'] = kwargs.get('autocommit', constants.DEFAULT_AUTOCOMMIT)
        self.db_conf['ssl'] = kwargs.get('ssl', None)

    def connect(self):
        self.connection = connect(
            db=self.db_conf['database'],
            host=self.db_conf['host'],
            port=self.db_conf['port'],
            user=self.db_conf['user'],
            password=self.db_conf['password'],
            charset=self.db_conf['charset'],
            ssl=self.db_conf['ssl'])

        self.connection.autocommit(self.db_conf["autocommit"])

        return self

    def cursor(self, cursor_class=None):
        if cursor_class:
            return self.connection.cursor(cursor_class)
        else:
            return self.connection.cursor(constants.DEFAULT_DB_CURSOR)

    def set_autocommit(self, value: bool):
        self.connection.autocommit(value)

    def rollback(self):
        self.connection.rollback()

    def commit(self):
        return self.connection.commit()

    def is_open(self):
        return self.connection.open

    def end(self):
        self.connection.close()
