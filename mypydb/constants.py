from mypydb.cursors import DictCursor


DEFAULT_CHARSET = 'utf8'
DEFAULT_DB_PORT = 3306
DEFAULT_AUTOCOMMIT = True
DEFAULT_DB_CURSOR = DictCursor
