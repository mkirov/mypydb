from pymysql.cursors import DictCursor
from pymysql.cursors import SSCursor
from pymysql.cursors import SSDictCursor
from pymysql.cursors import Cursor


class DefaultCursorUtils:

    def query(self, stmt: str, params: dict = None):

        if params is None:
            params = []

        try:
            self.execute(stmt, params)
        except UnicodeDecodeError:
            raise

        return self

    def query_many(self, stmt: str, params: list = None):

        if params is None:
            params = []

        try:
            self.executemany(stmt, params)
        except UnicodeDecodeError:
            raise

        return self

    def insert_or_update(self, table: str, data: dict, keys: list, **kwargs):
        return self.query(
            self._build_insert_or_update(table, data, keys, **kwargs), data)

    def insert_or_update_many(self, table: str, data: dict, keys: list, **kwargs):
        return self.query_many(
            self._build_insert_or_update(table, data[0], keys, **kwargs), data)

    def insert(self, table: str, data: dict):
        return self.query(
            self._build_insert(table, data), data)

    def insert_many(self, table: str, data: list):
        return self.query_many(
            self._build_insert(table, data[0]), data)

    def update(self, table: str, data: dict, keys: list):
        return self.query(
            self._build_update(table, data, keys), data)

    def update_many(self, table: str, data: list, keys: list):
        return self.query_many(
            self._build_update(table, data[0], keys), data)

    def delete(self, table: str, data: dict, keys: list):
        return self.query(
            self._build_delete(table, data, keys), data)

    def delete_many(self, table: str, data: list, keys: list):
        return self.query(
            self._build_delete(table, data[0], keys), data)

    def _build_insert_or_update(self, table: str, data: dict, keys: list, ignore_update: list = None, **kwargs):
        insert_columns, insert_values = self._serialize_insert(data)

        if ignore_update:
            update_set = self._serialize_update(
                {k: v for k, v in data.items() if k not in ignore_update},
                keys)
        else:
            update_set = self._serialize_update(data, keys)

        sql = """INSERT INTO {} ({})
                 VALUES({}) ON DUPLICATE KEY
                 UPDATE {}""".format(
            table, insert_columns,
            insert_values, update_set)

        return sql

    def _build_insert(self, table: str, data: dict):
        insert_columns, insert_values = self._serialize_insert(data)
        sql = "INSERT INTO {} ({}) VALUES({})".format(
            table, insert_columns, insert_values)

        return sql

    def _build_update(self, table: str, data: dict, keys: list):
        set_values = self._serialize_update(data, keys)
        sql = "UPDATE {} SET {} WHERE {}".format(
            table, set_values,
            self._build_where_condition(data, keys))

        return sql

    def _build_delete(self, table: str, data: dict, keys: list):
        sql = "DELETE FROM {} WHERE {}".format(
            table, self._build_where_condition(data, keys))

        return sql

    def _build_where_condition(self, data, keys):
        if not keys:
            raise MissingWhereCondition('Missing keys for update query')

        where_condition = list()
        for key in keys:
            if key not in data:
                raise MissingWhereCondition('Missing key {} in data'.format(key))

            if isinstance(data[key], list):
                where_condition.append(' IN '.join((key, '%({})s'.format(key))))
            else:
                where_condition.append('='.join((key, '%({})s'.format(key))))

        if not where_condition:
            raise MissingWhereCondition(
                'Invalid where condition: missing keys in data')

        return ' AND '.join(where_condition)

    def _serialize_update(self, data: dict, keys: list):
        return ', '.join(
            ['='.join(
                [k, '%({})s'.format(k)])for k in data.keys() if k not in keys])

    def _serialize_insert(self, data: dict):
        return (
            ','.join(data.keys()),
            ','.join(["%({})s".format(k) for k, v in data.items()]))


class DictCursor(DictCursor, DefaultCursorUtils):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class Cursor(Cursor, DefaultCursorUtils):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class SSCursor(SSCursor, DefaultCursorUtils):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class SSCursor(SSDictCursor, DefaultCursorUtils):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class MissingWhereCondition(RuntimeError):
    pass

