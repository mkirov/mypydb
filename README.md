# MYPYDB

To use with db connections config file need to create environment variable: DB_CONNECTIONS_CONFIG with path to your yaml database config file:

    DB_CONNECTIONS_CONFIG=/my/database/file.yml
  
A connections config file can also be set manually by doing:

```python
    import mypydb

    mypydb.set_conn_config('/my/database/file.yml')

```

File structure of database config should be like:

```yaml
    NAME_FOR_THE_DB_CONFIG:
     host: <DB_HOST>
     user: <DB_USER>
     password: <DB_PASSWORD>
     database: <DB_DATABASE>
     charset: <DB_CHARSET>
    ANOTHER_NAME_FOR_ANOTHER_DB_CONFIG:
     host: <DB_HOST>
     user: <DB_USER>
     password: <DB_PASSWORD>
     database: <DB_DATABASE>
     charset: <DB_CHARSET>
```


# Connect to a db


If there is not connections config file bind to the variable, mypydb has an option to manually connect to a db.
Example:

```python
    import mypydb

    db_conn = mypydb.connect(database=<DB_NAME>,
                             host=<DB_HOST>,
                             port=<DB_PORT>,
                             user=<DB_USER>,
                             password=<DB_PASSWORD>,
                             charset=<DB_CHARSET>)
```


If there is config that mydbpy can use then you can connect to a db simple by passing it's name:

```python
    import mypydb

    db_conn = mypydb.connect(<NAME_FOR_THE_DB_CONFIG>)
``` 


Example of usage:

```python

    import mypydb

    db_conn = mypydb.connect('NAME_FOR_THE_DB_CONFIG')

    with db_conn.cursor() as cursor:   # default cursor is DictCursor  
        qry = cursor.query('''
            SELECT 
                something
            FROM
                somewhere 
            WHERE
                some_id = %(some_id)s
                AND created_by <> %(user)s''',
            dict(some_id=44444,
                 user='some_user'))

        qry_rows = qry.fetchall()  
```




