import setuptools

setuptools.setup(
    name="mypydb", # Replace with your own username
    version="1.0",
    author="Mario Kirov",
    author_email="mkirov@isimarkets.com",
    description="database config",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
